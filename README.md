# The kayak project

Ce nouveau projet va faire de nous les leaders sur le marché du _Palindrome as a Service_ (PaaS). Notre API fournira de façon ouverte un moyen de verifier que n'importe quel mot est un palindrome. La disponibilité, la performance et l'UX seront bien sur des facteurs clés de succès !

Pour le _business model_ on verra quand on aura levé des fonds...

Notre choix de technologie s'est porté sur [Flask](http://flask.pocoo.org) pour le _framework_, [Docker](https://www.docker.com) pour le _run_ et [GitLab](https://gitlab.com/mdiehr/kayak) pour la gestion de source et l'intégration continue. Le CEO a déjà bootstrappé le projet avec un `Dockerfile` et l'installation du _framework_ (`create_virtualenv.sh` et `requirements.txt`)

Lancez le script `./create_virtualenv.sh` pour installer le virtualenv dans un dossier `venv` puis `. venv/bin/activate` pour l'activer.

L'objectif du _sprint_ zéro :
 - Construire le hello-world
 - Mettre en place la CI
