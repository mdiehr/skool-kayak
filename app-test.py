#!/usr/bin/env python3

import unittest
from app import app

class HelloWorld(unittest.TestCase):
    def test_hello_world(self):
        result = app.test_client(self)

        response = result.get("/", content_type = "html/text")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b"Hello World")

if __name__ == "__main__":
    unittest.main()
